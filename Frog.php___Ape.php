<?php

require_once('animal.php');

class Ape extends animal
{
    public $legs = 2;
    public $name = "Kera Sakti";
    public function yell()
    {
        return "Yell : Auooo <br><br>";
    }
}

class Frog extends animal
{
    public $name = "Buduk";
    public function jump()
    {
        return "Jump : Hop Hop <br><br>";
    }
}

?>